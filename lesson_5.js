var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Задание 1. Список студентов
console.log("Список студентов:");
for(var i=0, massLength=studentsAndPoints.length; i<massLength; i+=2){
    console.log("Студент %s набрал %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);
}

//Задание 2. Студент, набравший максимальный балл
console.log("\nСтудент, набравший максимальный балл:");
var maxCount=-1;
var maxName;
for(var i=0, arrayLength=studentsAndPoints.length; i<arrayLength; i+=2){
    if(studentsAndPoints[i+1]>maxCount){
        maxCount = studentsAndPoints[i+1];
        maxName = studentsAndPoints[i];
    }
}
console.log("Студент %s имеет максимальный балл %d", maxName, maxCount);

//Задание 3. Новые студенты
studentsAndPoints.push('Николай Фролов',0,'Олег Боровой',0);

//Задание 4. 2 студента получили по 10 баллов
var index = studentsAndPoints.indexOf('Антон Павлович');
if(index !== -1)
    studentsAndPoints[index+1]+=10;
index = studentsAndPoints.indexOf('Николай Фролов');
if(index !== -1)
    studentsAndPoints[index+1]+=10;

// Замечание
index = studentsAndPoints.indexOf('Дмитрий Фитискин');
if(index !== -1)
    studentsAndPoints[index+1]+=10;

//Задание 5. Список студентов, не набравших баллов
console.log("\nСтуденты, не набравшие баллов:");
for(var i=0, massLength=studentsAndPoints.length; i<massLength; i+=2){
    if(studentsAndPoints[i+1]===0)
        console.log(studentsAndPoints[i]);
}

//Дополнительное задание. Исключение студентов, не набравших баллов
for(var i=0, massLength=studentsAndPoints.length; i<massLength; i+=2){
    if(studentsAndPoints[i+1]===0){
        studentsAndPoints.splice(i,2);
        i-=2;
        massLength-=2;
    }
}

console.log("\nСписок студентов:");
for(var i=0, massLength=studentsAndPoints.length; i<massLength; i+=2){
    console.log("Студент %s набрал %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);
}